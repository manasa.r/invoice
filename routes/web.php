<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin','HomeController@admin')->name('admin');
Route::get('/updateuser','HomeController@updateUser')->name('update_user');

Route::get('/newregister','HomeController@newregister');
Route::post('/newuserregister','HomeController@newuserregister')->name('newuserregister');