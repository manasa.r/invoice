<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use DB;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //$roles = Roles::all();
       //$users = DB::table('users')->paginate(5);


       //$selectedRole = User::first()->role_id;

    //return view('my_view', compact('roles', 'selectedRole');
     //}
        //}
        
    }
    public function admin()
    {
      //  dd($roles);
         $user = Auth::user();
          //$users = DB::table('users')->get()->paginate(5);
         $users = DB::table('users')->paginate(7);
          $roles = DB::table('roles')->get();
         
            return view('admin')->with('users',$users)->with('roles',$roles);
        
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'role'=> 'required|string|max:255'
        ]);
    }

        public function newregister()
        {
        $user = Auth::user();
        return view('newregister');
        }


        
        public function newuserregister(Request $request)
        {
          $this->validator($request->all())->validate();

          event(new Registered($user = $this->create($request->all())));
          if($user){
            return redirect()->route('admin')->with('success','User registered successfully');
          }

        }
    

    protected function create(array $data)
    {
        //$role=2;
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'role'=>$data['role'],
            
        ]);
    }

    public function updateUser($id,$request)
    {
        
    $user = Task::findOrFail($id);

    $this->validate($request, [
        'name' => 'required',
        'email' => 'required',
        'role' => 'required'

    ]);

    $user = $request->all();

    $user->fill($input)->save();

    Session::flash('flash_message', 'updated successfully !');

    return redirect()->back();
    }
}
